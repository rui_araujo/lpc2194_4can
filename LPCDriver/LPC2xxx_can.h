/*
 * LPC2xxx_can.h
 *
 *  Created on: Aug 13, 2014
 *      Author: raraujo
 */

#ifndef LPC2XXX_CAN_H_
#define LPC2XXX_CAN_H_

#include "LPC2xxx.h"

#define SJW_DEFAULT			(0b0)
#define TESG1_DEFAULT		(0b1100)
#define TESG2_DEFAULT		(0b001)

//Mod bits
#define RM		(1<<0)
#define LOM		(1<<1)
#define STM		(1<<2)
#define TPM		(1<<3)
#define SM		(1<<4)
#define RPM		(1<<5)
#define TM		(1<<7)

//GSR bits
#define RBS		(1<<0)
#define DOS		(1<<1)
#define TBS		(1<<2)
#define TCS		(1<<3)
#define RS		(1<<4)
#define TS		(1<<5)
#define ES		(1<<6)
#define BS		(1<<7)

//CMR bit
#define TR		(1<<0)
#define AT		(1<<1)
#define RRB		(1<<2)
#define CDO		(1<<3)
#define SRR		(1<<4)
#define STB1	(1<<5)
#define STB2	(1<<6)
#define STB3	(1<<7)

#define DLC_MASK (0xF)
#define SJW_MASK (0x3)
#define TESG1_MASK (0xF)
#define TESG2_MASK (0x7)
#define SAM_MASK (0x1)
#define BRP_MASK (0x3FF)

static inline void CAN_Enable(CAN_Type *can) {
	can->MOD &= ~RM;
}

static inline void CAN_Disable(CAN_Type *can) {
	can->MOD |= RM;
}

static inline void CAN_StartTX(CAN_Type *can) {
	can->CMR |= TR;
}

static inline void CAN_ReleaseBuffer(CAN_Type *can) {
	can->CMR |= RRB;
}

static inline uint8_t CAN_MessageReceived(CAN_Type *can) {
	return can->GSR & RBS;
}

static inline uint8_t CAN_TransmitterEmpty(CAN_Type *can) {
	return can->GSR & TBS;
}
static inline uint8_t CAN_ErrorStatus(CAN_Type *can) {
	return can->GSR & ES;
}

static inline uint8_t CAN_BufferTransmitterEmpty(CAN_Type *can, uint8_t buffer) {
	return can->SR & (1 << (2 + buffer * 8));
}

static inline uint8_t CAN_BufferTransmitionComplete(CAN_Type *can,
		uint8_t buffer) {
	return can->SR & (1 << (3 + buffer * 8));
}

static inline void CAN_setTxDLC(CAN_Type *can, uint8_t txBuffer, uint8_t length) {
	can->TX[txBuffer].TFI |= (length & DLC_MASK) << 16;
}

static inline uint32_t CAN_setBTR(CAN_Type *can, uint32_t bitrate, uint8_t SJW,
		uint8_t TESG1, uint8_t TESG2, uint8_t SAM) {
	TESG1 &= TESG1_MASK;
	TESG2 &= TESG2_MASK;
	uint32_t prescaler = SystemAPBFrequency / (TESG1 + TESG2 + 3);
	prescaler /= bitrate;
	can->BTR = ((SAM & SAM_MASK) << 23) | (TESG2 << 20) | (TESG1 << 16)
			| ((SJW & SJW_MASK) << 14) | (((prescaler - 1) & BRP_MASK) << 0);
	return SystemAPBFrequency / ((TESG1 + TESG2 + 3) * (prescaler + 1));
}

static inline void CAN_BypassFilter(){
	CAN_FILTER->AFMR |= 0x2;
}

static inline void CAN_EnableFilter(){
	CAN_FILTER->AFMR &= ~0x3;
}

static inline void CAN_DisableFilter(){
	CAN_FILTER->AFMR |= 0x1;
}

#endif /* LPC2XXX_CAN_H_ */
