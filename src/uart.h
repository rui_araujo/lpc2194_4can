#ifndef __UART_H 
#define __UART_H
#include <stdbool.h>
#include "ring_buffer.h"
#include "LPC2xxx_fio.h"
#include "pinout.h"


//Transmit buffer that will be used on the rest of the system
extern RINGBUFF_T uart0TX;
extern RINGBUFF_T uart0RX;

extern volatile bool clearToSend; //status of the CTS flag

#define CTS_BUFFER_THRESHOLD		128

/**
 * It initializes the selected UART peripheral.
 * @param UARTx Pointer to the UART peripheral
 * @param Baudrate Baud rate to be used
 */
extern void UARTInit(void);

/**
 * Prints through the UART the version string of the firmware.
 */
extern void UARTShowVersion(void);

/**
 * Parses the character received
 * @param newChar new character received
 */
extern void UART0ParseNewChar();

/*
 * It there is chars in the transmit buffer, and transmission stopped
 * because CTS was not asserted, this function should be called to restart transmission
 */
extern void UARTRestartTX();

static inline void processUART() {
	if (RingBuffer_GetCount(&uart0RX)) {
		UART0ParseNewChar();
		if (RingBuffer_GetFree(&uart0RX) < CTS_BUFFER_THRESHOLD) {
			//Please stop!
			FIO_SetPins(FIO0, _BIT(UART0_RTS_PIN));
		} else {
			//Ready to do business!
			FIO_ClearPins(FIO0, _BIT(UART0_RTS_PIN));
		}
	}

	if (FIO_ReadPins(FIO0, _BIT(UART0_CTS_PIN)) == 0) { // no rts stop signal
		if (!clearToSend && !RingBuffer_IsEmpty(&uart0TX)) {
			UARTRestartTX();
		}
	}
}

#endif /* end __UART_H */
/*****************************************************************************
 **                            End Of File
 ******************************************************************************/
