#include <string.h>
#include "uart.h"
#include "lpc_types.h"
#include "timers.h"
#include "LPC2xxx_uart.h"
#include "LPC2xxx_fio.h"
#include "LPC2xxx_vic.h"
#include "system_LPC2xxx.h"
#include "LPC2xxx_pinsel.h"
#include "LPC2194_pinconfig.h"
#include "pinout.h"
#include "xprintf.h"
#include "can.h"
#include <stdbool.h>
#include <ctype.h>

#define SOFTWARE_VERSION 		"1.0"

#define DEFAULT_BAUDRATE_UART0	(4000000)

#define TX_BUFFER_SIZE_BITS		(12)
#define TX_BUFFER_SIZE			(1<<TX_BUFFER_SIZE_BITS)

#define RX_BUFFER_SIZE_BITS		(12)
#define RX_BUFFER_SIZE			(1<<RX_BUFFER_SIZE_BITS)

//Transmit buffer that will be used on the rest of the system
RINGBUFF_T uart0TX;
RINGBUFF_T uart0RX;

volatile bool clearToSend = false;

char uart0TxBuffer[TX_BUFFER_SIZE];
char uart0RxBuffer[RX_BUFFER_SIZE];

#define UART_COMMAND_LINE_MAX_LENGTH  	128

// *****************************************************************************

unsigned char commandLine[UART_COMMAND_LINE_MAX_LENGTH];
uint32_t commandLinePointer;
uint32_t enableUARTecho;	 // 0-no cmd echo, 1-only cmd reply, 2-all visible

// *****************************************************************************
#define UARTReturn()	   xputc('\n')

/* The rate at which data is sent to the queue, specified in milliseconds. */

/* UART transmit-only interrupt handler for ring buffers */
static inline void UART_TXIntHandlerFlowControl() {
	uint8_t ch;

	/* Fill FIFO until full or until TX ring buffer is empty */
	if (FIO_ReadPins(FIO0, _BIT(UART0_CTS_PIN)) == 0) { // no rts stop signal
		clearToSend = true;
		while ((UART_GetLineStatus(UART0) & UART_LineStatus_TxEmpty) != 0 && RingBuffer_Pop(&uart0TX, &ch)) {
			UART_Send(UART0, ch);
		}
	} else {
		clearToSend = false;
	}
}

/* UART receive-only interrupt handler for ring buffers */
static inline void UART_RXIntHandlerRB(UART_Type *pUART, RINGBUFF_T *pRB) {
	/* New data will be ignored if data not popped in time */
	while (UART_GetLineStatus(pUART) & UART_LineStatus_RxData) {
		uint8_t ch = UART_Recv(pUART);
		RingBuffer_Insert(pRB, &ch);
	}
}

void UARTRestartTX() {
	/* Don't let UART transmit ring buffer change in the UART IRQ handler */
	UART_DisableIT(UART0, UART_THRE_INT_ENA);
	UART_TXIntHandlerFlowControl();
	/* Enable UART transmit interrupt */
	UART_EnableIT(UART0, UART_THRE_INT_ENA);
}

void UART0WriteChar(char pcBuffer) {

	uint32_t ret;
	do {
		/* Don't let UART transmit ring buffer change in the UART IRQ handler */
		UART_DisableIT(UART0, UART_THRE_INT_ENA);

		/* Move as much data as possible into transmit ring buffer */
		ret = RingBuffer_Insert(&uart0TX, &pcBuffer);
		UART_TXIntHandlerFlowControl();

		/* Enable UART transmit interrupt */
		UART_EnableIT(UART0, UART_THRE_INT_ENA);
	} while (ret == 0);
}

void UART0_IRQHandler(void) __attribute__ ((interrupt ("IRQ")));
void UART0_IRQHandler(void) {
	uint32_t interruptIdentifier;
	/* Handle transmit interrupt if enabled */
	if (UART0->IIR & UART_ITID_TxEmpty) {
		UART_TXIntHandlerFlowControl();

		/* Disable transmit interrupt if the ring buffer is empty */
		if (RingBuffer_IsEmpty(&uart0TX)) {
			UART_DisableIT(UART0, UART_THRE_INT_ENA);
		}
	}
	while (((interruptIdentifier = UART0->IIR) & 0x01) == 0) {
		if (interruptIdentifier & UART_ITID_RxDataAvailable) {
			/* Handle receive interrupt */
			UART_RXIntHandlerRB(UART0, &uart0RX);
#if 0
			if (RingBuffer_GetFree(&uart0RX) < CTS_BUFFER_THRESHOLD) {
				//Please stop!
				FIO_SetPins(FIO0, _BIT(UART0_RTS_PIN));
			} else {
				//Ready to do business!
				FIO_ClearPins(FIO0, _BIT(UART0_RTS_PIN));
			}
#endif
		}
	}
	VIC_IRQDone();
}

/* Determines and sets best dividers to get a target baud rate */
uint32_t UART_SetBaudFDR(UART_Type *pUART, uint32_t baudrate)

{
	uint32_t uClk = SystemCoreClock;
	uint32_t actualRate = 0, d, m, bestd, bestm, tmp;
	uint32_t current_error, best_error;
	uint64_t best_divisor, divisor;
	uint32_t recalcbaud;

	/* In the Uart IP block, baud rate is calculated using FDR and DLL-DLM registers
	 * The formula is :
	 * BaudRate= uClk * (mulFracDiv/(mulFracDiv+dividerAddFracDiv) / (16 * (DLL)
	 * It involves floating point calculations. That's the reason the formulae are adjusted with
	 * Multiply and divide method.*/
	/* The value of mulFracDiv and dividerAddFracDiv should comply to the following expressions:
	 * 0 < mulFracDiv <= 15, 0 <= dividerAddFracDiv <= 15 */
	best_error = 0xFFFFFFFF;/* Worst case */
	bestd = 0;
	bestm = 0;
	best_divisor = 0;
	for (m = 1; m <= 15; m++) {
		for (d = 0; d < m; d++) {

			/*   The result here is a fixed point number.  The integer portion is in the upper 32 bits.
			 * The fractional portion is in the lower 32 bits.
			 */
			divisor = ((uint64_t) uClk << 28) * m / (baudrate * (m + d));

			/*   The fractional portion is the error. */
			current_error = divisor & 0xFFFFFFFF;

			/*   Snag the integer portion of the divisor. */
			tmp = divisor >> 32;

			/*   If closer to the next divisor... */
			if (current_error > ((uint32_t) 1 << 31)) {

				/* Increment to the next divisor... */
				tmp++;

				/* Now the error is the distance to the next divisor... */
				current_error = -current_error;
			}

			/*   Can't use a divisor that's less than 1 or more than 65535. */
			if ((tmp < 1) || (tmp > 65535)) {
				/* Out of range */
				continue;
			}

			/*   Also, if fractional divider is enabled can't use a divisor that is less than 3. */
			if ((d != 0) && (tmp < 3)) {
				/* Out of range */
				continue;
			}

			/*   Do we have a new best? */
			if (current_error < best_error) {
				best_error = current_error;
				best_divisor = tmp;
				bestd = d;
				bestm = m;

				/*   If error is 0, that's perfect.  We're done. */
				if (best_error == 0) {
					break;
				}
			}
		} /* for (d) */

		/*   If error is 0, that's perfect.  We're done. */
		if (best_error == 0) {
			break;
		}
	} /* for (m) */

	if (best_divisor == 0) {
		/* can not find best match */
		return 0;
	}

	recalcbaud = (uClk >> 4) * bestm / (best_divisor * (bestm + bestd));

	/* reuse best_error to evaluate baud error */
	if (baudrate > recalcbaud) {
		best_error = baudrate - recalcbaud;
	} else {
		best_error = recalcbaud - baudrate;
	}

	best_error = (best_error * 100) / baudrate;
	if (best_error > 3)
		return 0;

	/* Update UART registers */
	UART_SetDTR(pUART, best_divisor);

	/* Return actual baud rate */
	actualRate = recalcbaud;

	return actualRate;
}

void UARTInit() {
	memset(commandLine, 0, UART_COMMAND_LINE_MAX_LENGTH);
	commandLinePointer = 0;
	enableUARTecho = 1;
	RingBuffer_Init(&uart0RX, uart0RxBuffer, 1, RX_BUFFER_SIZE);
	RingBuffer_Init(&uart0TX, uart0TxBuffer, 1, TX_BUFFER_SIZE);
	xdev_out(UART0WriteChar);
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_0_TXD0);
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_1_RXD0);
	PINSEL_SetPinFunction(UART0_RTS_PIN, PINSEL_Function_Default);
	PINSEL_SetPinFunction(UART0_CTS_PIN, PINSEL_Function_Default);
	FIO_SetPinDirections(FIO0, _BIT(UART0_RTS_PIN), FIO_Direction_Out);
	FIO_SetPinDirections(FIO0, _BIT(UART0_CTS_PIN), FIO_Direction_In);
	FIO_ClearPins(FIO0, _BIT(UART0_RTS_PIN));
	UART_SetWordLength(UART0, UART_WordLength_8b);
	UART_SetStopBits(UART0, UART_StopBits_1);
	UART_SetParity(UART0, UART_Parity_No);
	UART0->IER = 0;
	UART_EnableIT(UART0, UART_RBR_INT_ENA);
	UART_SetRxFifoTrigger(UART0, UART_RxFifoTrigger_14);
	UART_SetBaudFDR(UART0, DEFAULT_BAUDRATE_UART0);
	UART_EnableFifos(UART0);
	UART_EnableTx(UART0);
	VIC_SetSlot(0, UART0_IRQn, UART0_IRQHandler);
	VIC_EnableSlot(0);
	//VIC_EnableFIQ(UART0_IRQn);
	VIC_EnableIRQ(UART0_IRQn);
}

// *****************************************************************************

// *****************************************************************************
void UARTShowVersion(void) {
	xputs("\nCan4_LPC2194, V" SOFTWARE_VERSION " " __DATE__ ", " __TIME__ "\n");
}

// *****************************************************************************
static void UARTShowUsage(void) {

	UARTShowVersion();

	UARTReturn();
	xputs("Supported Commands:\n");
	UARTReturn();
	xputs(" !IMx=cmd_id,data_id     - register motor x CAN ids\n");
	xputs(" !ISx=data_id            - register sensor x CAN id\n");
	UARTReturn();

	xputs(" !DMx+b,p                - enable motor streaming, ??DM to show options\n");
	xputs(" !DMx-[b]                - disable sensors streaming, ??DM to show options\n");
	xputs(" !DSx+b,p                - enable sensor streaming, ??DS to show options\n");
	xputs(" !DSx-[b]                - disable sensors streaming, ??DS to show options\n");
	UARTReturn();

	xputs(" !Mx=arg                 - send pwm command to motor x\n");
	UARTReturn();

	xputs(" !U=x                    - set baud rate to x\n");
	xputs(" !U[0,1,2]               - UART echo mode (none, cmd-echo, all)\n");
	UARTReturn();

	xputs(" ??                      - display (this) help\n");
	UARTReturn();
}

// *****************************************************************************
static uint32_t parseUInt32(unsigned char **c) {
	uint32_t ul = 0;
	while (((**c) >= '0') && ((**c) <= '9')) {
		ul = 10 * ul;
		ul += ((**c) - '0');
		(*(c))++;
	}
	return (ul);
}

static int32_t parseInt32(unsigned char **c) {
	if ((**c) == '-') {
		(*(c))++;
		return (-1 * ((int32_t) parseUInt32(c)));
	}
	if ((**c) == '+') {
		(*(c))++;
	}
	return ((int32_t) parseUInt32(c));
}

static inline void UARTShowSensorOptions(void) {
	xputs("Bitlist for available sensors:\n");
	xputs(" Bit Dec-Value Name     # Values  Description\n");
	xputs(" 0   1         JOINT         1    joint angle\n");
}

static inline void UARTShowMotorReadingOptions(void) {
	xputs("Bitlist for available sensors:\n");
	xputs(" Bit Dec-Value Name     # Values  Description\n");
	xputs(" 0   1         OMEGA         1    omega\n");
	xputs(" 1   2         ENCODER       1    encoder position\n");
	xputs(" 2   4         CURRENT       1    motor current\n");
	xputs(" 3   8         DISPLACEMENT  1    displacement\n");
}

// *****************************************************************************
// * ** parseGetCommand ** */
// *****************************************************************************
static void UARTParseGetCommand(void) {

	switch (commandLine[1]) {

	case '?':
		if (commandLine[2] == 'D' || commandLine[2] == 'd') {
			if (commandLine[3] == 'M' || commandLine[3] == 'm') {
				UARTShowMotorReadingOptions();
				break;
			} else if (commandLine[3] == 'S' || commandLine[3] == 's') {
				UARTShowSensorOptions();
				break;
			}
		}
		UARTShowUsage();
		break;
	default:
		xputs("Get: parsing error\n");
	}
}

static inline void processDataCommand(unsigned char *c, bool isMotor) {
	int id = parseUInt32(&c);
	bool flag = 0;
	if (*c == '+') {
		flag = true;
	} else if (*c == '-') {
		flag = false;
	} else {
		xputs("Set: parsing error\n");
		return;
	}
	c++;
	if (!isdigit(*c)) {
		if (!flag && *c == '\0') {
			if (isMotor) {
				stopMotorData(id, 0xFF);
			} else {
				stopSensorData(id, 0xFF);
			}
			return;
		}
		xputs("Set: parsing error\n");
		return;
	}
	uint32_t mask = parseUInt32(&c);
	if (*c == ',') {
		c++;
	} else {
		if (flag) {
			xputs("Set: parsing error\n");
			return; //second argument only mandatory when enabling.
		}
	}
	uint32_t period = 1;
	if (flag) {
		period = parseUInt32(&c);
		if (isMotor) {
			requestMotorData(id, mask, period);
		} else {
			requestSensorData(id, mask, period);
		}
	} else {
		if (isMotor) {
			stopMotorData(id, mask);
		} else {
			stopSensorData(id, mask);
		}
	}
}

// *****************************************************************************
// * ** parseSetCommand ** */
// *****************************************************************************
static void UARTParseSetCommand(void) {
	switch (commandLine[1]) {

	case 'D':
	case 'd': {
		unsigned char *c = commandLine + 2;
		if ((*c == 'M') || (*c == 'm')) {
			c++;
			processDataCommand(c, true);
			return;
		} else if ((*c == 'S') || (*c == 's')) {
			c++;
			processDataCommand(c, false);
			return;
		} else {
			xputs("Set: parsing error\n");
			return;
		}
	}
	case 'I':
	case 'i': {
		unsigned char *c = commandLine + 2;
		if ((*c == 'M') || (*c == 'm')) {
			c++;
			int id = parseUInt32(&c);
			if (*c != '=') {
				xputs("Set: parsing error\n");
				return;
			}
			c++;
			int controlId = parseUInt32(&c);
			if (*c != ',') {
				xputs("Set: parsing error\n");
				return;
			}
			c++;
			int dataId = parseUInt32(&c);
			if (registerMotor(id, controlId, dataId)) {
				xputs("error registering\n");
			}
			return;
		} else if ((*c == 'S') || (*c == 's')) {
			c++;
			int id = parseUInt32(&c);
			if (*c != '=') {
				xputs("Set: parsing error\n");
				return;
			}
			c++;
			int dataId = parseUInt32(&c);
			if (registerSensor(id, dataId)) {
				xputs("Error registering\n");
			}
			return;
		} else {
			xputs("Set: parsing error\n");
			return;
		}
	}
	case 'M':
	case 'm': {
		unsigned char *c = commandLine + 2;
		int id = parseUInt32(&c);
		if (*c != '=') {
			xputs("Set: parsing error\n");
			return;
		}
		c++;
		int arg = parseInt32(&c);
		if (sendPWMCommand(id, arg)) {
			xputs("Error sending command\n");
		}
		return;
	}
	case 'U':
	case 'u': {
		unsigned char *c;
		long baudRate;
		c = commandLine + 2;
		if (((*c) >= '0') && ((*c) <= '2')) {
			enableUARTecho = ((*c) - '0');
			break;
		}
		c++;
		baudRate = parseUInt32(&c);
		while ((UART0->LSR & UART_LineStatus_TxEmpty) == 0) {
		};		   // wait for UART to finish data transfer
		xprintf("-U=%d\n", baudRate);
		delayMs(100);
		if (UART_SetBaudFDR(UART0, baudRate) == 0) {
			xprintf("Failed to switch Baud Rate to %d Baud!\n", baudRate);
		}
		break;
	}

	default:
		xputs("Set: parsing error\n");
	}
}

// *****************************************************************************
// * ** parseRS232CommandLine ** */
// *****************************************************************************
static void parseRS232CommandLine(void) {

	switch (commandLine[0]) {
	case '?':
		UARTParseGetCommand();
		break;
	case '!':
		UARTParseSetCommand();
		break;
	default:
		xputs("?\n");
	}
}

// *****************************************************************************
// * ** RS232ParseNewChar ** */
// *****************************************************************************
void UART0ParseNewChar() {
	unsigned char newChar;
	RingBuffer_Pop(&uart0RX, &newChar);
	switch (newChar) {
	case 8:			// backspace
		if (commandLinePointer > 0) {
			commandLinePointer--;
			if (enableUARTecho) {
				xprintf("%c %c", 8, 8);
			}
		}
		break;

	case 10:
	case 13:
		if (enableUARTecho) {
			UARTReturn();
		}
		if (commandLinePointer > 0) {
			commandLine[commandLinePointer] = 0;
			parseRS232CommandLine();
			commandLinePointer = 0;
		}
		break;

	default:
		if (newChar & 0x80) {
			return; //only accept ASCII
		}
		if (commandLinePointer < UART_COMMAND_LINE_MAX_LENGTH - 1) {
			if (enableUARTecho) {
				xputc(newChar);	  		   	// echo to indicate char arrived
			}
			commandLine[commandLinePointer++] = newChar;
		} else {
			commandLinePointer = 0;
			commandLine[commandLinePointer++] = newChar;
		}
	}  // end of switch

}  // end of rs232ParseNewChar

