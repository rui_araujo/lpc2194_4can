/*
 * packet.h
 *
 *  Created on: Aug 19, 2014
 *      Author: raraujo
 */

#ifndef PACKET_H_
#define PACKET_H_

#include <stdint.h>

#define TOP_BIT			(1<<7)
#define SOURCE_MASK		(0x1)
#define INDEX_MASK		(0xF)
#define TYPE_MASK		(0x3)

#define USE_CHEKSUM		(0)
#if USE_CHEKSUM
#define HEADER_POS	1
#define HEADER_TYPE uint16_t
#define PACKET_SIZE	6
#else
#define HEADER_POS	0
#define HEADER_TYPE uint8_t
#define PACKET_SIZE	5
#endif


/**
 * The packet has a fixed length of 6 bytes with checksum or just 5 bytes without checksum
 * 0 - 0b1010cccc - fixed 4 upper bits and 4 bit checksum
 * 1 - 0b1tddddss - t    - source flag: motor monitor (cleared), sensor (set)
 * 					dddd - 4 bits for index (0..7) of data source
 * 					ss - specifies type of sensory data (only relevant for motor monitor)
 * 					 	 3: displacement, 2: current, 1: encoder Position, 0: omega
 * 	The following 4 bytes are the payload in int32 format.
 */
typedef union {
	unsigned char data[PACKET_SIZE];
	struct {
		HEADER_TYPE header;
		uint32_t payload;
	}__attribute__ ((__packed__)) s;
} packet_t;

static inline void setPacketHeader(packet_t * p, uint8_t source, uint8_t id,
		uint8_t sensorType) {
	p->data[HEADER_POS] = TOP_BIT | ((source & SOURCE_MASK) << 6)
			| ((id & INDEX_MASK) << 2) | ((sensorType & TYPE_MASK) << 0);
}

void sendPacket(packet_t *);

#endif /* PACKET_H_ */
