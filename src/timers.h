/*
 * sensors.h
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#ifndef TIMERS_H_
#define TIMERS_H_
#include <stdint.h>
#include <stdbool.h>

#define MAX_TIMERS							(96)

typedef struct {
	volatile uint8_t triggered; /* Flag which is set to 1 when counter reaches zero. Must be cleared manually.*/
	int16_t position; /* Position in the enabled sensors queue */
	volatile uint32_t reload; /* Reload value for the counter */
	volatile uint32_t counter; /* Counter which is decremented every ms*/
	void (*cb)(void *); /* pointer to the refresh function where the values are printed*/
	void * arg;
} timer_t;

struct {
	volatile bool refreshRequested; //Flag set to one when a possible refresh of the sensors is required
	timer_t timers[MAX_TIMERS]; //Sensor array
	timer_t * enabledTimers[MAX_TIMERS]; //Enabled sensor queue
	uint32_t enabledTimersCounter; //Counter of the enabled sensors
	volatile uint32_t timerDelay; //To be used for timer delays
} softTimers;

/**
 * Initializes the sensor array with pointers for their functions
 * and it sets up the Systick interupt which will be used as a timer.
 */
extern void timersInit(void);

/**
 * It enabled or disabled a single sensor.
 * @param sensorId Number between 0 and 31
 * @param flag ENABLE or DISABLE
 * @param period the period used for the print out
 * @param the callback
 */
extern int16_t addTimer(uint32_t period, void (*cb)(void *), void * arg);
void updateTimer(int16_t timerId, uint32_t period, void (*cb)(void *),
		void * arg);
extern void removeTimer(int16_t timerId);

extern void delayMs(uint32_t delay);

/**
 * Invokes the refresh function of each sensor manually.
 * @param mask bitfield where each bit corresponds to a different sensor
 */
extern void runTimersCB(uint8_t mask);

static inline void processSoftTimers() {
	if (softTimers.refreshRequested) {
		softTimers.refreshRequested = 0;
		for (int i = 0; i < softTimers.enabledTimersCounter; ++i) {
			if (softTimers.enabledTimers[i]->triggered) {
				softTimers.enabledTimers[i]->cb(
						softTimers.enabledTimers[i]->arg);
				softTimers.enabledTimers[i]->triggered = 0;
			}
		}
	}
}

#endif /* TIMERS_H_ */
