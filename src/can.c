/*
 * can.c
 *
 *  Created on: Aug 12, 2014
 *      Author: raraujo
 */
#include "LPC2194.h"
#include "LPC2xxx_can.h"
#include "LPC2xxx_pinsel.h"
#include "LPC2194_pinconfig.h"
#include "can.h"
#include "packet.h"
#include "timers.h"
#include <string.h>

#define CAN_BAUDRATE	(1000000)

#define MAX_MOTORS		(16)
#define MAX_SENSORS		(16)

//Motor fields
#define MOTOR_SOURCE			(0)
#define MOTOR_FIELDS			(4)
#define OMEGA_SENSOR			(0)
#define ENCODER_SENSOR			(1)
#define CURRENT_SENSOR			(2)
#define DISPLACEMENT_SENSOR		(3)

//Sensor fields
#define SENSOR_SOURCE			(1)
#define SENSOR_FIELDS			(1)
#define JOINT_SENSOR			(0)

#define TIMEOUT_COUNTER			(100000)

typedef struct motor {
	uint8_t id;
	int16_t commandId;
	int16_t dataId1;
	int16_t dataId2;
	int16_t current;
	int16_t displacement;
	int32_t encoderPosition;
	union {
		int32_t omega;
		float omegaBadDesign;
	};
	union {
		int32_t dutyCycle;
		float dutyCycleBadDesign;
	};
	int16_t timersID[MOTOR_FIELDS];
	CAN_Type * canController;
	struct motor *next;
} motor_t;

typedef struct sensor {
	uint8_t id;
	int16_t dataId;
	int16_t jointPosition;
	int16_t timersID[SENSOR_FIELDS];
	struct sensor * next;
} sensor_t;

motor_t motors[MAX_MOTORS];
sensor_t sensors[MAX_SENSORS];

motor_t * registeredMotors = NULL;
sensor_t * registeredSensors = NULL;

typedef union {
	unsigned char value[4];
	int16_t valueShort[2];
	int32_t valueInt;
	float valueFloat;
} converter_t;

void CAN_Init(void) {
	CAN_Disable(CAN1);
	CAN_Disable(CAN2);
	CAN_Disable(CAN3);
	CAN_Disable(CAN4);


	//Resetting the error counters
	CAN1->GSR = 0;
	CAN2->GSR = 0;
	CAN3->GSR = 0;
	CAN4->GSR = 0;

	//The default value
	CAN_setBTR(CAN1, CAN_BAUDRATE, SJW_DEFAULT, TESG1_DEFAULT, TESG2_DEFAULT, 0);
	CAN_setBTR(CAN2, CAN_BAUDRATE, SJW_DEFAULT, TESG1_DEFAULT, TESG2_DEFAULT, 0);
	CAN_setBTR(CAN3, CAN_BAUDRATE, SJW_DEFAULT, TESG1_DEFAULT, TESG2_DEFAULT, 0);
	CAN_setBTR(CAN4, CAN_BAUDRATE, SJW_DEFAULT, TESG1_DEFAULT, TESG2_DEFAULT, 0);

	//Config Pins - TD1 is a dedicated pin
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_25_RD1);
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_23_RD2);
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_24_TD2);
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_21_RD3);
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_22_TD3);

	CAN_BypassFilter();

	CAN_Enable(CAN1);
	CAN_Enable(CAN2);
	CAN_Enable(CAN3);
	CAN_Enable(CAN4);
	for (int i = 0; i < MAX_MOTORS; ++i) {
		memset(motors + i, 0, sizeof(motor_t));
		motors[i].id = i;
		motors[i].commandId = -1;
		motors[i].dataId1 = -1;
		motors[i].dataId2 = -1;
		motors[i].canController = NULL;
		for (int j = 0; j < MOTOR_FIELDS; ++j) {
			motors[i].timersID[j] = -1;
		}
	}
	for (int i = 0; i < MAX_SENSORS; ++i) {
		memset(sensors + i, 0, sizeof(sensor_t));
		sensors[i].id = i;
		sensors[i].dataId = -1;
		for (int j = 0; j < SENSOR_FIELDS; ++j) {
			sensors[i].timersID[j] = -1;
		}
	}
}

status_t registerMotor(uint16_t id, uint16_t canID_for_control, uint16_t canID_for_data) {
	if (id >= MAX_MOTORS) {
		return INVALID_ARG;
	}
	if (motors[id].commandId == -1) { //Never registered before
		motors[id].next = registeredMotors;
		registeredMotors = &motors[id];
	}
	motors[id].commandId = canID_for_control;
	motors[id].dataId1 = canID_for_data;
	motors[id].dataId2 = canID_for_data + 1;
	motors[id].canController = NULL;
	return OK;
	//TODO:register ids in cAN controllers
}
status_t registerSensor(uint16_t id, uint16_t canID) {
	if (id >= MAX_SENSORS) {
		return INVALID_ARG;
	}
	if (sensors[id].dataId == -1) { //Never registered before
		sensors[id].next = registeredSensors;
		registeredSensors = &sensors[id];
	}
	sensors[id].dataId = canID;
	sensors[id].jointPosition = 0;
	return OK;
	//TODO:register ids in cAN controllers
}

void reportCurrent(void *arg) {
	motor_t * motor = (motor_t*) arg;
	packet_t packet = { { 0 } };
	setPacketHeader(&packet, MOTOR_SOURCE, motor->id, CURRENT_SENSOR);
	packet.s.payload = motor->current;
	sendPacket(&packet);
}
void reportDisplacement(void *arg) {
	motor_t * motor = (motor_t*) arg;
	packet_t packet = { { 0 } };
	setPacketHeader(&packet, MOTOR_SOURCE, motor->id, DISPLACEMENT_SENSOR);
	packet.s.payload = motor->displacement;
	sendPacket(&packet);

}
void reportEncoderPosition(void *arg) {
	motor_t * motor = (motor_t*) arg;
	packet_t packet = { { 0 } };
	setPacketHeader(&packet, MOTOR_SOURCE, motor->id, ENCODER_SENSOR);
	packet.s.payload = motor->encoderPosition;
	sendPacket(&packet);

}
void reportOmega(void *arg) {
	motor_t * motor = (motor_t*) arg;
	packet_t packet = { { 0 } };
	setPacketHeader(&packet, MOTOR_SOURCE, motor->id, OMEGA_SENSOR);
	packet.s.payload = motor->omega;
	sendPacket(&packet);
}
void reportJointPosition(void *arg) {
	sensor_t * sensor = (sensor_t*) arg;
	packet_t packet = { { 0 } };
	setPacketHeader(&packet, SENSOR_SOURCE, sensor->id, JOINT_SENSOR);
	packet.s.payload = sensor->jointPosition;
	sendPacket(&packet);

}

void requestMotorData(uint16_t id, uint8_t dataField, uint32_t period) {
	if (id >= MAX_MOTORS) {
		return;
	}
	if (motors[id].dataId1 == -1 || motors[id].dataId2 == -1) {
		return;
	}
	if (dataField & _BIT(OMEGA_SENSOR)) {
		if (motors[id].timersID[OMEGA_SENSOR] == -1) {
			motors[id].timersID[OMEGA_SENSOR] = addTimer(period, reportOmega, &motors[id]);
		} else {
			updateTimer(motors[id].timersID[OMEGA_SENSOR], period, reportOmega, &motors[id]);
		}
	}
	if (dataField & _BIT(ENCODER_SENSOR)) {
		if (motors[id].timersID[ENCODER_SENSOR] == -1) {
			motors[id].timersID[ENCODER_SENSOR] = addTimer(period, reportEncoderPosition, &motors[id]);
		} else {
			updateTimer(motors[id].timersID[ENCODER_SENSOR], period, reportEncoderPosition, &motors[id]);
		}
	}
	if (dataField & _BIT(CURRENT_SENSOR)) {
		if (motors[id].timersID[CURRENT_SENSOR] == -1) {
			motors[id].timersID[CURRENT_SENSOR] = addTimer(period, reportCurrent, &motors[id]);
		} else {
			updateTimer(motors[id].timersID[CURRENT_SENSOR], period, reportCurrent, &motors[id]);
		}
	}
	if (dataField & _BIT(DISPLACEMENT_SENSOR)) {
		if (motors[id].timersID[DISPLACEMENT_SENSOR] == -1) {
			motors[id].timersID[DISPLACEMENT_SENSOR] = addTimer(period, reportDisplacement, &motors[id]);
		} else {
			updateTimer(motors[id].timersID[DISPLACEMENT_SENSOR], period, reportDisplacement, &motors[id]);
		}
	}
}

void stopMotorData(uint16_t id, uint8_t dataField) {
	if (id >= MAX_MOTORS) {
		return;
	}
	if (dataField & _BIT(OMEGA_SENSOR)) {
		removeTimer(motors[id].timersID[OMEGA_SENSOR]);
		motors[id].timersID[OMEGA_SENSOR] = -1;
	}
	if (dataField & _BIT(ENCODER_SENSOR)) {
		removeTimer(motors[id].timersID[ENCODER_SENSOR]);
		motors[id].timersID[ENCODER_SENSOR] = -1;
	}
	if (dataField & _BIT(CURRENT_SENSOR)) {
		removeTimer(motors[id].timersID[CURRENT_SENSOR]);
		motors[id].timersID[CURRENT_SENSOR] = -1;
	}
	if (dataField & _BIT(DISPLACEMENT_SENSOR)) {
		removeTimer(motors[id].timersID[DISPLACEMENT_SENSOR]);
		motors[id].timersID[DISPLACEMENT_SENSOR] = -1;
	}
}
void requestSensorData(uint16_t id, uint8_t dataField, uint32_t period) {
	if (id >= MAX_SENSORS) {
		return;
	}
	if (sensors[id].dataId == -1) {
		return;
	}
	if (dataField & _BIT(JOINT_SENSOR)) {
		if (sensors[id].timersID[JOINT_SENSOR] == -1) {
			sensors[id].timersID[JOINT_SENSOR] = addTimer(period, reportJointPosition, &sensors[id]);
		} else {
			updateTimer(sensors[id].timersID[JOINT_SENSOR], period, reportJointPosition, &sensors[id]);
		}
	}
}
void stopSensorData(uint16_t id, uint8_t dataField) {
	if (id >= MAX_SENSORS) {
		return;
	}
	if (dataField & JOINT_SENSOR) {
		removeTimer(sensors[id].timersID[JOINT_SENSOR]);
		sensors[id].timersID[JOINT_SENSOR] = -1;
	}
}

static void flushCANCommand(uint8_t length, uint16_t commandId, uint32_t tda, uint32_t tdb) {
	CAN1->MOD |= STM;
	CAN2->MOD |= STM;
	CAN3->MOD |= STM;
	CAN4->MOD |= STM;
	for (int i = 0; i < 4; ++i) {
		CAN_Type * canController;
		switch (i) {
		case 0:
			canController = CAN1;
			break;
		case 1:
			canController = CAN2;
			break;
		case 2:
			canController = CAN3;
			break;
		case 3:
			canController = CAN4;
			break;
		}

		int timetout = TIMEOUT_COUNTER;
		while (!CAN_BufferTransmitterEmpty(canController, 0)) {
			__asm volatile("nop");
			if (timetout-- == 0) {
				return;
			}
		}
		canController->CMR |= STB1; //Enable the TX Buffer 1
		canController->TX[0].TFI = 0;
		CAN_setTxDLC(canController, 0, length);
		canController->TX[0].TID = commandId;
		canController->TX[0].TDA = tda;
		canController->TX[0].TDB = tdb;
		CAN_StartTX(canController);
		timetout = TIMEOUT_COUNTER;
		while (!CAN_BufferTransmitionComplete(canController, 0)) {
			__asm volatile("nop");
			if (CAN_ErrorStatus(canController)) {

				break;
			}
			if (timetout-- == 0) {
				break;
			}
		}
	}
	CAN1->MOD &= ~STM;
	CAN2->MOD &= ~STM;
	CAN3->MOD &= ~STM;
	CAN4->MOD &= ~STM;
}

status_t sendPWMCommand(uint8_t id, int32_t dutycycle) {
	if (id >= MAX_MOTORS) {
		return INVALID_ARG;
	}
	if (motors[id].commandId == -1) {
		return INVALID_ARG;
	}
	if (motors[id].canController == NULL) {
		converter_t con;
		con.valueFloat = (float) dutycycle;
		flushCANCommand(4, motors[id].commandId, con.valueInt, 0);
		return OK;
	}
	int timetout = TIMEOUT_COUNTER;
	while (!CAN_BufferTransmitterEmpty(motors[id].canController, 0)) {
		__asm volatile("nop");
		if (timetout-- == 0) {
			return TIMEOUT;
		}
	}
	motors[id].canController->CMR |= STB1; //Enable the TX Buffer 1
	motors[id].canController->TX[0].TFI = 0;
	CAN_setTxDLC(motors[id].canController, 0, 4);
	motors[id].canController->TX[0].TID = motors[id].commandId;
	converter_t con;
	con.valueFloat = (float) dutycycle;
	motors[id].canController->TX[0].TDA = con.valueInt;
	CAN_StartTX(motors[id].canController);
#ifdef DEBUG
	timetout = TIMEOUT_COUNTER;
	while (!CAN_BufferTransmitionComplete(motors[id].canController, 0)) {
		__asm volatile("nop");
		if (CAN_ErrorStatus(motors[id].canController)) {
			return GENERAL_ERROR;
		}
		if (timetout-- == 0) {
			return TIMEOUT;
		}
	}
#endif
	return OK;
}

void parseCANData(CAN_Type *can) {
	int16_t id = can->RID;
	//searching for struct with this id
	motor_t * currentMotor = registeredMotors, *motorFound = NULL;
	while (currentMotor != NULL) {
		if (currentMotor->dataId2 == id || currentMotor->dataId1 == id) {
			motorFound = currentMotor;
			break;
		}
		currentMotor = currentMotor->next;
	}
	if (motorFound != NULL) {
		motorFound->canController = can; //Updating the can controller
		if (id == motorFound->dataId1) {
			converter_t con;
			con.valueInt = can->RDA;
			motorFound->omega = (int32_t) (con.valueFloat * 1000);
			motorFound->encoderPosition = can->RDB;
		} else { //id == motorFound->dataId2
			int32_t data = can->RDA;
			motorFound->current = (int16_t) (data & 0xFFFF);
			motorFound->displacement = (int16_t) ((data >> 16) & 0xFFFF);
		}
		CAN_ReleaseBuffer(can);
		return;
	}
	//Motor not found, looking for sensors
	sensor_t * currentSensor = registeredSensors, *sensorFound = NULL;
	while (currentSensor != NULL) {
		if (currentSensor->dataId == id) {
			sensorFound = currentSensor;
			break;
		}
		currentSensor = currentSensor->next;
	}
	if (sensorFound == NULL) {
		CAN_ReleaseBuffer(can);
		return; //Found nothing
	}
	int32_t data = can->RDA;
#ifdef CRAZY_CONVERSIONS_FOR_SENSORS_NOT_ENABLED
	if (data & 0x0800) {
		data |= 0xFFFFF000; // fix myorobot's ill int12 data format
	}
#endif
	sensorFound->jointPosition = data & 0xFFFF;
	CAN_ReleaseBuffer(can);
}
