/*
 * pinout.h
 *
 *  Created on: Aug 14, 2014
 *      Author: raraujo
 */

#ifndef PINOUT_H_
#define PINOUT_H_

#define UART0_RX_PORT		0
#define UART0_RX_PIN		1

#define UART0_TX_PORT		0
#define UART0_TX_PIN		0

#define UART0_CTS_PORT		0
#define UART0_CTS_PIN		5

#define UART0_RTS_PORT		0
#define UART0_RTS_PIN		6

#define CAN1_RX_PORT		0
#define CAN1_RX_PIN			25

//TD1 is on a dedicated pin

#define CAN2_RX_PORT		0
#define CAN2_RX_PIN			23

#define CAN2_TX_PORT		0
#define CAN2_TX_PIN			24

#define CAN3_RX_PORT		0
#define CAN3_RX_PIN			21

#define CAN3_TX_PORT		0
#define CAN3_TX_PIN			22

#define LED_PORT			0
#define LED_PIN				16

#endif /* PINOUT_H_ */
