/*
 * packet.c
 *
 *  Created on: Aug 19, 2014
 *      Author: raraujo
 */

#include "packet.h"
#include "xprintf.h"
#include <stdlib.h>

#define DEBUG_PACKET 0

#if USE_CHEKSUM

#define HEADER_CHECKSUM		0xA0
/*Compute 4-bit 1's complement checksum of 64 bits. Sum is returned in
 top 4 bits of result & other bits are zero. */
uint32_t chksum_64(uint32_t header, uint32_t payload) {
	register uint32_t checksum __asm__("r0"); //make sure that the checksum is returned correctly
	__asm__ __volatile__(
			"adds    r0, r0, r1\n\t"             // ; s = a + b
			"it		 cs\n\t"
			"addcs   r0, r0, #1 \n\t"//             ; Add back carry
			"adds    r0, r0, r0, lsl #16\n\t"//     ; s = s + s << 16;
			"it		 cs\n\t"
			"addcs   r0, r0, #0x00010000\n\t"//     ; Add back carry
			"bic     r0, r0, #0x0000ff00 \n\t"//    ; Ensure no carry in
			"adds    r0, r0, r0, lsl #8 \n\t"//     ; s = s + s << 8
			"it		 cs\n\t"
			"addcs   r0, r0, #0x01000000 \n\t"//    ; Add back carry
			"bic     r0, r0, #0x00ff0000 \n\t"//    ; Ensure no carry in
			"adds    r0, r0, r0, lsl #4  \n\t"//    ; s = s + s << 4
			"it		 cs\n\t"
			"addcs   r0, r0, #0x10000000\n\t"//     ; Add back carry
			"and     r0, r0, #0xf0000000 \n\t"//    ; Isolate checksum
			"eor     r0, r0, #0xf0000000 \n\t"//    ; Complement top 4
	);
	return checksum;
}
#endif

void sendPacket(packet_t * packet) {
	if (packet == NULL) {
		return;
	}
#if USE_CHEKSUM
	packet->data[0] = HEADER_CHECKSUM;
	uint32_t checksum = chksum_64(packet->s.header, packet->s.payload);
	packet->data[0] |= (unsigned char) (checksum >> 28);
	checksum = chksum_64(packet->s.header, packet->s.payload);
#endif
#if DEBUG_PACKET
#if USE_CHEKSUM
	xprintf("%02X %02X", packet->data[0], packet->data[1]);
#else
	xprintf("%02X", packet->data[0]);
#endif
	xprintf(" %08LX\n", packet->s.payload);
#else
	for (int i = 0; i < PACKET_SIZE; ++i) {
		xputc(packet->data[i]);
	}
#endif
}
