/*
 . * sensors.c
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#include "timers.h"
#include "xprintf.h"
#include <stdbool.h>
#include "LPC2xxx_timer.h"
#include "LPC2xxx_vic.h"

/**
 * The Systick handler is used for a lot more tasks than sensor timing.
 * It also provides a timer for decaying for the motor velocity, motor control
 * and second timer used for the LED blinking and Retina event rate.
 */
volatile uint8_t toggleLed0 = 0;
void TIMER0_Handler(void) __attribute__ ((interrupt ("IRQ")));
void TIMER0_Handler(void) {
	if (softTimers.timerDelay) {
		--softTimers.timerDelay;
	}
	for (int i = 0; i < softTimers.enabledTimersCounter; ++i) {
		if (--softTimers.enabledTimers[i]->counter == 0) {
			softTimers.enabledTimers[i]->counter = softTimers.enabledTimers[i]->reload;
			softTimers.enabledTimers[i]->triggered = 1;
			softTimers.refreshRequested = 1;
		}
	}
	TIMER_ClearPendingIT(TIMER0, TIMER_IT_MR0);
	VIC_IRQDone();
}

void timersInit(void) {
	softTimers.refreshRequested = 0;
	softTimers.timerDelay = 0;
	softTimers.enabledTimersCounter = 0;
	for (int i = 0; i < MAX_TIMERS; ++i) {
		softTimers.enabledTimers[i] = NULL;
		softTimers.timers[i].triggered = false;
		softTimers.timers[i].reload = 0;
		softTimers.timers[i].counter = 0;
		softTimers.timers[i].position = -1;
		softTimers.timers[i].arg = NULL;
		softTimers.timers[i].cb = NULL;
	}
	TIMER_AssertReset(TIMER0);
	TIMER_Disable(TIMER0);
	TIMER0->IR = 0;
	TIMER0->EMR = 0;
	TIMER0->PR = 0;
	TIMER0->CTCR = 0;
	TIMER0->CCR = 0;
	VIC_SetSlot(1, TIM0_IRQn, TIMER0_Handler);
	VIC_EnableSlot(1);
	VIC_EnableIRQ(TIM0_IRQn);
	TIMER_SetChannelMatchValue(TIMER0, 0, SystemAPBFrequency / 1000 - 1);
	TIMER_SetMode(TIMER0, TIMER_Mode_Timer);
	TIMER_SetChannelMatchControl(TIMER0, 0,
	TIMER_MatchControl_Interrupt | TIMER_MatchControl_Reset);
	TIMER_DeassertReset(TIMER0);
	TIMER_Enable(TIMER0);
}
void delayMs(uint32_t delay) {
	softTimers.timerDelay = delay;
	while (softTimers.timerDelay) {
		__asm volatile("nop");
	}
}

void updateTimer(int16_t timerId, uint32_t period, void (*cb)(void *), void * arg) {
	if (cb == NULL) {
		return;
	}
	if (softTimers.timers[timerId].position == -1) {
		return;
	}
	TIMER_Disable(TIMER0);	//disable the Timer0
	softTimers.timers[timerId].counter = period;	//Update the period
	softTimers.timers[timerId].reload = period;	//Update the period
	softTimers.timers[timerId].arg = arg;
	softTimers.timers[timerId].cb = cb;
	TIMER_Enable(TIMER0);	//enable the Timer0
}

int16_t addTimer(uint32_t period, void (*cb)(void *), void * arg) {
	if (softTimers.enabledTimersCounter >= MAX_TIMERS) {
		return -2;
	}
	if (cb == NULL) {
		return -1;
	}
	TIMER_Disable(TIMER0);	//disable the Timer0
	for (int i = 0; i < MAX_TIMERS; ++i) {
		if (softTimers.timers[i].position == -1) {
			softTimers.timers[i].counter = period;
			softTimers.timers[i].reload = period;
			softTimers.enabledTimers[softTimers.enabledTimersCounter++] = &softTimers.timers[i];
			softTimers.timers[i].position = softTimers.enabledTimersCounter - 1;
			softTimers.timers[i].arg = arg;
			softTimers.timers[i].cb = cb;
			TIMER_Enable(TIMER0);	//enable the Timer0
			return i;
		}
	}
	TIMER_Enable(TIMER0);	//enable the Timer0
	return -2; // Shouldn't get here
}
void removeTimer(int16_t timerId) {
	if (timerId >= MAX_TIMERS || timerId < 0) {
		return;
	}
	if (softTimers.timers[timerId].position != -1) {
		TIMER_Disable(TIMER0);	//disable the Timer0
		if (softTimers.timers[timerId].position == softTimers.enabledTimersCounter - 1) {
			softTimers.enabledTimers[softTimers.timers[timerId].position] = NULL;
		} else {
			//Move last to the current position
			int currentPosition = softTimers.timers[timerId].position;
			softTimers.enabledTimers[currentPosition] = softTimers.enabledTimers[softTimers.enabledTimersCounter - 1];
			softTimers.enabledTimers[currentPosition]->position = currentPosition;
			softTimers.enabledTimers[softTimers.enabledTimersCounter - 1] = NULL;
		}
		softTimers.timers[timerId].position = -1;
		softTimers.timers[timerId].triggered = 0;
		softTimers.enabledTimersCounter--;
		TIMER_Enable(TIMER0);	//enable the Timer0
	}
}

void runTimersCB(uint8_t mask) {
	for (int i = 0; i < MAX_TIMERS; ++i) {
		if (mask & (1 << i)) {
			if (softTimers.timers[i].cb != NULL) {
				softTimers.timers[i].cb(softTimers.timers[i].arg);
			}
		}
	}
}
