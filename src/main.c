/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */
#include "system_LPC2xxx.h"
#include "LPC2xxx_fio.h"
#include "LPC2194_pinconfig.h"
#include "LPC2xxx_pinsel.h"
#include "LPC2xxx_can.h"
#include "pinout.h"
#include "can.h"
#include "uart.h"
#include "timers.h"
#include "xprintf.h"
#include <stdbool.h>

#define LED_PERIOD		(1000)

void toggleLed(void *arg) {
	static bool ledState = true;
	if (ledState) {
		FIO_SetPins(FIO0, _BIT(LED_PIN));
	} else {
		FIO_ClearPins(FIO0, _BIT(LED_PIN));
	}
	ledState = !ledState;
}

int main(void) {
	SystemInit();
	timersInit();
	UARTInit();
	CAN_Init();
	PINSEL_SetPinConfig(PINSEL_PinConfig_0_16_PIO);
	FIO_SetPinDirections(FIO0, _BIT(LED_PIN), FIO_Direction_Out);
	addTimer(LED_PERIOD, toggleLed, NULL);
	UARTShowVersion();
	while (1) {
		processUART();
		processSoftTimers();
		if (CAN_MessageReceived(CAN1)) {
			parseCANData(CAN1);
		}
		if (CAN_MessageReceived(CAN2)) {
			parseCANData(CAN2);
		}
		if (CAN_MessageReceived(CAN3)) {
			parseCANData(CAN3);
		}
		if (CAN_MessageReceived(CAN4)) {
			parseCANData(CAN4);
		}
	}
}
