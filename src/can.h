#ifndef CAN_H_
#define CAN_H_
#include <stdint.h>

typedef enum {
	OK= 0, INVALID_ARG, TIMEOUT, GENERAL_ERROR
} status_t;

void CAN_Init(void);
status_t registerMotor(uint16_t id, uint16_t canID_for_control,
		uint16_t canID_for_data);
status_t registerSensor(uint16_t id, uint16_t canID);

void requestMotorData(uint16_t id, uint8_t dataField, uint32_t period);
void stopMotorData(uint16_t id, uint8_t dataField);

void requestSensorData(uint16_t id, uint8_t dataField, uint32_t period);
void stopSensorData(uint16_t id, uint8_t dataField);

status_t sendPWMCommand(uint8_t id, int32_t dutycycle);

void parseCANData(CAN_Type *can);

#endif
